from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from sales_rest.models import Salesperson, Customer, Sale, AutomobileVO
from sales_rest.encoders import (
    SalespersonEncoder,
    SaleEncoder,
    CustomerEncoder
)


@require_http_methods(["GET", "POST"])
def api_list_salespeople(request):
    if request.method == "GET":
        try:
            salespeople = Salesperson.objects.all()
            return JsonResponse(
                {"salespeople": salespeople},
                encoder=SalespersonEncoder,
            )
        except Salesperson.DoesNotExist:
            return JsonResponse(
                {"message": "There are no salespeople."},
                status=400,
            )
    else:
        content = json.loads(request.body)

        salesperson = Salesperson.objects.create(**content)
        return JsonResponse(
            salesperson,
            encoder=SalespersonEncoder,
            safe=False,
        )


@require_http_methods(["DELETE", "PUT", "GET"])
def api_show_salesperson(request, employee_id):
    if request.method == "GET":
        try:
            salesperson = Salesperson.objects.get(employee_id=employee_id)
            return JsonResponse(
                salesperson,
                encoder=SalespersonEncoder,
                safe=False,
            )
        except Salesperson.DoesNotExist:
            return JsonResponse(
                {"message": "Employee does not exist."},
                status=404,
            )
    elif request.method == "DELETE":
        try:
            salesperson = Salesperson.objects.get(employee_id=employee_id)
            count, _ = Salesperson.objects.filter(employee_id=employee_id).delete()
            return JsonResponse(
                {"message": "Salesperson has been deleted."}
            )
        except Salesperson.DoesNotExist:
            return JsonResponse(
                {"message": "Employee does not exist."},
                status=404,
            )
    else:
        content = json.loads(request.body)
        Salesperson.objects.filter(employee_id=employee_id).update(**content)
        salesperson = Salesperson.objects.get(employee_id=employee_id)
        return JsonResponse(
            salesperson,
            encoder=SalespersonEncoder,
            safe=False,
        )


@require_http_methods(["GET", "POST"])
def api_list_customers(request):
    if request.method == "GET":
        try:
            customers = Customer.objects.all()
            return JsonResponse(
                {"customers": customers},
                encoder=CustomerEncoder,
            )
        except Customer.DoesNotExist:
            return JsonResponse(
                {"message": "There are no customers."},
                status=400,
            )
    else:
        content = json.loads(request.body)

        customer = Customer.objects.create(**content)
        return JsonResponse(
            customer,
            encoder=CustomerEncoder,
            safe=False,
        )


@require_http_methods(["DELETE", "PUT", "GET"])
def api_show_customer(request, id):
    if request.method == "GET":
        try:
            customer = Customer.objects.get(id=id)
            return JsonResponse(
                customer,
                encoder=CustomerEncoder,
                safe=False,
            )
        except Customer.DoesNotExist:
            return JsonResponse(
                {"message": "Customer does not exist."},
                status=404,
            )
    elif request.method == "DELETE":
        try:
            customer = Customer.objects.get(id=id)
            count, _ = Customer.objects.filter(id=id).delete()
            return JsonResponse(
                {"deleted": "Customer has been deleted."},
                )
        except Customer.DoesNotExist:
            return JsonResponse(
                {"message": "Customer does not exist."},
                status=404
            )
    else:
        content = json.loads(request.body)
        Customer.objects.filter(id=id).update(**content)
        customer = Customer.objects.get(id=id)
        return JsonResponse(
            customer,
            encoder=CustomerEncoder,
            safe=False,
        )


@require_http_methods(["GET", "POST"])
def api_list_sales(request):
    if request.method == "GET":
        try:
            sales = Sale.objects.all()
            return JsonResponse(
                {"sales": sales},
                encoder=SaleEncoder,
                safe=False
            )
        except Sale.DoesNotExist:
            return JsonResponse(
                {"message": "There are no sales."},
                status=400,
            )
    else:
        content = json.loads(request.body)

        try:
            auto_vin = content["auto"]
            auto = AutomobileVO.objects.get(vin=auto_vin)
            content["auto"] = auto
        except AutomobileVO.DoesNotExist:
            return JsonResponse(
                {"message": "Automobile does not exist."},
                status=400,
            )
        try:
            salesperson = content["salesperson"]
            employee = Salesperson.objects.get(employee_id=salesperson)
            content["salesperson"] = employee
        except Salesperson.DoesNotExist:
            return JsonResponse(
                {"message": "Employee does not exist"},
                status=400
            )
        try:
            customer = content["customer"]
            customer_id = Customer.objects.get(id=customer)
            content["customer"] = customer_id
        except Customer.DoesNotExist:
            return JsonResponse(
                {"message": "Customer does not exist"},
                status=400
            )

        sale = Sale.objects.create(**content)
        return JsonResponse(
            sale,
            encoder=SaleEncoder,
            safe=False,
        )


@require_http_methods(["GET", "DELETE"])
def api_show_sale(request, id):
    if request.method == "GET":
        try:
            sale = Sale.objects.get(id=id)
            return JsonResponse(
                sale,
                encoder=SaleEncoder,
                safe=False
            )
        except Sale.DoesNotExist:
            return JsonResponse(
                {"message": "Sale does not exist."}
            )
    else:
        try:
            count, _ = Sale.objects.get(id=id).delete()
            return JsonResponse({"message": "Sale was deleted."})
        except Sale.DoesNotExist:
            return JsonResponse({"message": "Sale does not exist."})
