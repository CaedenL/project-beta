from common.json import ModelEncoder
from sales_rest.models import Salesperson, Customer, Sale, AutomobileVO


class AutoVOEncoder(ModelEncoder):
    model: AutomobileVO
    properties = ["vin"]


class SalespersonEncoder(ModelEncoder):
    model = Salesperson
    properties = [
        "first_name",
        "last_name",
        "employee_id",
    ]


class CustomerEncoder(ModelEncoder):
    model = Customer
    properties = [
        "id",
        "first_name",
        "last_name",
        "address",
        "phone_number",
    ]


class SaleEncoder(ModelEncoder):
    model = Sale
    properties = [
        "id",
        "price",
        "salesperson",
        "customer"
    ]

    encoders = {
        "salesperson": SalespersonEncoder(),
        "customer": CustomerEncoder()
        }

    def get_extra_data(self, o):
        return {"auto": o.auto.vin}
