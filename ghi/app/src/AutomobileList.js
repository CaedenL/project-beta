import React, { useState, useEffect } from "react";


function AutomobileList() {

    const [autos, setAutos] = useState([]);

    const fetchData = async () => {
        const autoUrl = "http://localhost:8100/api/automobiles/";
        const response = await fetch(autoUrl);

        if (response.ok )  {
            const data = await response.json();
            setAutos(data.autos);
        } else {
            alert("Failed to load Automobile Data.")
        }

    };

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <div className="container my-3">
        <h1>Automobiles</h1>
        <table className="table table-bordered table-striped">
        <thead>
            <tr>
                <th>VIN</th>
                <th>Color</th>
                <th>Year</th>
                <th>Model</th>
                <th>Manufacturer</th>
                <th>Sold</th>
            </tr>
        </thead>
        <tbody>
            {autos.map(auto => {
                return (
                    <tr key={auto.id}>
                        <td>{ auto.vin }</td>
                        <td>{ auto.color }</td>
                        <td>{ auto.year}</td>
                        <td>{ auto.model.name }</td>
                        <td>{ auto.model.manufacturer.name }</td>
                        <td>{ auto.sold? "YES" : "NO" }</td>
                        <td><button onClick={() => {
                        const Url = `http://localhost:8100/api/automobiles/${auto.vin}/`
                        fetch(Url, { method: "DELETE"}).then((response) => {
                        if(!response.ok){
                            throw new Error('Something went wrong');
                        }
                        window.location.reload(false);
                        })
                        .catch((e) => {
                            console.log(e);
                        });
                    }} className="btn btn-danger">Delete</button></td>
                    </tr>
                )
            })}
        </tbody>
        </table>
        </div>
    )
}


export default AutomobileList
