import React, { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';



export default function SalesList(){
    const [sales, setSales] = useState([]);

    const fetchData = async () => {
        const salesUrl = "http://localhost:8090/api/sales/";
        const response = await fetch(salesUrl);

        if (response.ok) {
            const data = await response.json();
            setSales(data.sales);
        }
    };

    useEffect(() => {
        fetchData();

    }, []);

    return (
        <div className="container my-3">
            <h1>Sales</h1>
            <table className="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th scope="col">Salesperson Employee ID</th>
                        <th scope="col">Salesperson Name</th>
                        <th scope="col">Customer</th>
                        <th scope="col">VIN</th>
                        <th scope="col">Price</th>
                    </tr>
                </thead>
                <tbody>
                    {sales.map(sale => {
                        return (
                                <tr key={sale.id}>
                                    <td>{ sale.salesperson.employee_id }</td>
                                    <td>{ sale.salesperson.first_name } { sale.salesperson.last_name }</td>
                                    <td>{ sale.customer.first_name } { sale.customer.last_name }</td>
                                    <td>{ sale.auto }</td>
                                    <td>${ sale.price }</td>
                                    <td><button onClick={() => {
                            const salesUrl = `http://localhost:8090/api/sales/${sale.id}/`
                            fetch(salesUrl, { method: "DELETE"}).then((response) => {
                            if(!response.ok){
                                throw new Error('Something went wrong');
                            }
                            window.location.reload(false);
                            })
                            .catch((e) => {
                                console.log(e);
                            });
                        }} className="btn btn-danger">Delete</button></td>
                                </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
    )
}
