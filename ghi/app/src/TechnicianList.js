import React, { useState, useEffect } from "react";

function TechnicianList() {

    const [technicians, setTechnicians] = useState([]);

    const fetchData = async () => {
        const technicianUrl = "http://localhost:8080/api/technicians";
        const response = await fetch(technicianUrl);

        if (response.ok) {
            const data = await response.json();
            setTechnicians(data.technicians);
        } else {
            alert("Failed to fetch technician data!")
        }
    };

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <div className="container my-3">
        <h1>Technicians</h1>
        <table className="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>Name (Last, First)</th>
                    <th>Employee ID</th>
                </tr>
            </thead>
            <tbody>
                {technicians.map(technician => {
                    return (
                        <tr key={technician.id}>
                            <td>{ technician.last_name + ", " + technician.first_name }</td>
                            <td>{ technician.employee_id }</td>
                        </tr>
                    )
                })}
            </tbody>
        </table>
        </div>
    )

}


export default TechnicianList
