import React, { useState, useEffect } from "react";

function AppointmentList() {

    const [appointments, setAppointments] = useState([]);

    const fetchData = async () => {
        const appointmentUrl = "http://localhost:8080/api/appointments";
        const response = await fetch(appointmentUrl);

        if (response.ok) {
            const data = await response.json();
            setAppointments(data.appointments.filter(appointment => appointment.status === "CREATED"));
        }
    };

    useEffect(() => {
        fetchData();
    }, []);

    const completeAppointment = async (id) => {
        const completeUrl = `http://localhost:8080/api/appointments/${id}/finish/`;
        const response = await fetch(completeUrl, { method: 'PUT' });

        if (response.ok) {
            setAppointments(appointments.filter(appointment => appointment.id !== id));
        }
    };

    const cancelAppointment = async (id) => {
        const cancelUrl = `http://localhost:8080/api/appointments/${id}/cancel/`;
        const response = await fetch(cancelUrl, { method: 'PUT' });

        if (response.ok) {
            setAppointments(appointments.filter(appointment => appointment.id !== id));
        } else {
            alert("Failed to Cancel appointment")
        }
    };

    return (
        <div className="container my-3">
        <h1>Active Service Appointments</h1>
        <table className="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>VIN</th>
                    <th>Is VIP?</th>
                    <th>Customer</th>
                    <th>Date & Time</th>
                    <th>Technician</th>
                    <th>Reason</th>
                </tr>
            </thead>
            <tbody>
                {appointments.map(appointment => {
                    return (
                        <tr key={appointment.id}>
                            <td>{ appointment.vin }</td>
                            <td>{ appointment.VIP }</td>
                            <td>{ appointment.customer }</td>
                            <td>{ new Date(appointment.date_time).toLocaleString() }</td>
                            <td>{ appointment.technician.first_name + " " + appointment.technician.last_name }</td>
                            <td>{ appointment.reason }</td>
                            <td>
                                <button className="btn btn-danger" onClick={() => cancelAppointment(appointment.id)}>CANCEL</button>
                            </td>
                            <td>
                                <button className="btn btn-success" onClick={() => completeAppointment(appointment.id)}>DONE</button>
                            </td>
                            {/* <td>{ appointment.status }</td> */}
                        </tr>
                    );
                })}
            </tbody>
        </table>
        </div>
    )
}



export default AppointmentList
