import React, { useState, useEffect } from 'react';


export default function SalespersonList(){

    const [salespeople, setSalesperson] = useState([]);

    const fetchData = async () => {
        const salespeopleUrl = "http://localhost:8090/api/salespeople/";
        const response = await fetch(salespeopleUrl);

        if (response.ok) {
            const data = await response.json();
            setSalesperson(data.salespeople);
        }
    };

    useEffect(() => {
        fetchData();

    }, []);

    return (
        <div className="container my-3">
            <h1>Salespeople</h1>
            <table className="table table-bordered table-striped">
                <thead>
                    <tr>
                    <th scope="col">Employee ID</th>
                        <th scope="col">First Name</th>
                        <th scope="col">Last Name</th>
                    </tr>
                </thead>
                <tbody>
                    {salespeople.map(salesperson => {
                        return (
                            <tr key={ salesperson.employee_id }>
                                <td>{ salesperson.employee_id }</td>
                                <td>{ salesperson.first_name }</td>
                                <td>{ salesperson.last_name }</td>
                                <td><button onClick={() => {
                            const Url = `http://localhost:8090/api/salespeople/${salesperson.employee_id}/`
                            fetch(Url, { method: "DELETE"}).then((response) => {
                            if(!response.ok){
                                throw new Error('Something went wrong');
                            }
                            window.location.reload(false);
                            })
                            .catch((e) => {
                                console.log(e);
                            });
                        }} className="btn btn-danger">Delete</button></td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
    )
}
