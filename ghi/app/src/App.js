import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import TechnicianList from './TechnicianList';
import TechnicianForm from './TechnicianForm';
import AppointmentList from './AppointmentList';
import AppointmentForm from './AppointmentForm';
import ServiceHistory from './ServiceHistory';
import CustomerList from './CustomerList';
import CustomerForm from './CustomerForm';
import SalespersonList from './SalespersonList';
import SalespersonForm from './SalespersonForm';
import SalesList from './SalesList';
import SalesForm from './SalesForm';
import SalesHistory from './SalesHistory';
import ManufacturerList from './ManufacturerList';
import ManufacturerForm from './ManufacturerForm';
import ModelList from './ModelList';
import ModelForm from './ModelForm';
import AutomobileList from './AutomobileList';
import AutomobileForm from './AutomobileForm';



export default function App(props) {
    return (
        <BrowserRouter>
            <Nav />
            <div className="container">
            <Routes>
                <Route path="/" element={<MainPage />} />
                <Route path="manufacturers">
                    <Route path="" element={<ManufacturerList manufacturers={ props.manufacturer } />} />
                    <Route path="new" element={<ManufacturerForm />} />
                </Route>
                <Route path="models">
                    <Route path="" element={<ModelList models={ props.model } />} />
                    <Route path="new" element={<ModelForm />} />
                </Route>
                <Route path="automobiles">
                    <Route path="" element={<AutomobileList Automobiles={ props.automobile } />} />
                    <Route path="new" element={<AutomobileForm />} />
                </Route>
                <Route path="salespeople">
                    <Route path="" element={<SalespersonList salespeople={ props.saleperson } />} />
                    <Route path="new" element={<SalespersonForm />} />
                </Route>
                <Route path="customers">
                    <Route path="" element={<CustomerList customers={ props.customer } />} />
                    <Route path="new" element={<CustomerForm />} />
                </Route>
                <Route path="sales">
                    <Route path="" element={<SalesList sales={ props.sale } />} />
                    <Route path="new" element={<SalesForm />} />
                    <Route path="history" element={<SalesHistory />} />
                </Route>
                <Route path="technicians">
                    <Route path="" element={<TechnicianList technicians={ props.technicians } />} />
                    <Route path="new" element={<TechnicianForm />} />
                </Route>
                <Route path="services">
                    <Route path="" element={<AppointmentList appointments={ props.appointments } />} />
                    <Route path="new" element={<AppointmentForm />} />
                </Route>
                <Route path="services">
                    <Route path="history" element={<ServiceHistory services={ props.services } />} />
                </Route>
            </Routes>
            </div>
</BrowserRouter>
);
}
