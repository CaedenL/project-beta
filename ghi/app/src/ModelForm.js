import React, { useState, useEffect } from "react";


function ModelForm() {

    const [manufacturers, setManufacturers] = useState([]);

    const [formData, setFormData] = useState({
        name: '',
        picture_url: '',
        manufacturer_id: '',
    })

    const handleFormChange = (event) => {
        const value = event.target.value;
        const inputName = event.target.name;
        setFormData({
            ...formData,
            [inputName]: value,
        })
    }

    const fetchData = async () => {
        const manufacturerUrl = 'http://localhost:8100/api/manufacturers/';
        const response = await fetch(manufacturerUrl);

        if (response.ok) {
            const data = await response.json();
            setManufacturers(data.manufacturers)
        } else {
            alert("Could not fetch manufacturer data!")
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    const handleSubmit = async (event) => {
        event.preventDefault();
        const modelUrl = 'http://localhost:8100/api/models/';
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            }
        }
        const response = await fetch(modelUrl, fetchConfig);
        if (response.ok) {
            setFormData({
                name: '',
                picture_url: '',
                manufacturer_id: '',
            });
        } else {
            alert("Failed to add a vehicle model!")
        }
    };



    return (
        <div className="row">
        <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
            <h1>Add a vehicle model</h1>
            <form onSubmit={handleSubmit} id="add-model-form">
                <div className="form-floating mb-3">
                    <input value={formData.name} onChange={handleFormChange} placeholder="Model Name" required type="text" name="name" id="name" className="form-control"/>
                    <label htmlFor="name">Model Name</label>
                </div>
                <div className="form-floating mb-3">
                    <input value={formData.picture_url} onChange={handleFormChange} placeholder="Picture URL" required type="url" name="picture_url" id="picture_url" className="form-control"/>
                    <label htmlFor="picture_url">Picture URL</label>
                </div>
                <div className="mb-3">
                    <select value={formData.manufacturer_id} onChange={handleFormChange} required name="manufacturer_id" id="manufacturer_id" className="form-select">
                        <option value="">Choose a manufacturer</option>
                        {manufacturers.map(manufacturer=> {
                            return (
                                <option key={manufacturer.id} value={manufacturer.id}>
                                    {manufacturer.name}
                                </option>
                            );
                        })}
                    </select>
                </div>
                <button className="btn btn-primary">Add</button>
            </form>
            </div>
        </div>
        </div>
    )

}


export default ModelForm
