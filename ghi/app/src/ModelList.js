import React, { useState, useEffect } from "react";

function ModelList() {

    const [models, setModels] = useState([]);

    const fetchData = async () => {
        const modelUrl = "http://localhost:8100/api/models/";
        const response = await fetch(modelUrl);

        if (response.ok) {
            const data = await response.json();
            setModels(data.models);
        } else {
            alert("Failed to fetch model data!")
        }
    };

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <div className="container my-3">
        <h1>Models</h1>
        <table className="table table-bordered table-striped">
        <thead>
            <tr>
                <th>Name</th>
                <th>Manufacturer</th>
                <th>Picture</th>
            </tr>
        </thead>
        <tbody>
            {models.map(model => {
                return (
                    <tr key={model.id}>
                        <td>{ model.name }</td>
                        <td>{ model.manufacturer.name }</td>
                        <td>
                            <img src={ model.picture_url } alt={ model.name } style={ {width: "100px"} } />
                        </td>
                        <td><button onClick={() => {
                        const Url = `http://localhost:8100/api/models/${model.id}/`
                        fetch(Url, { method: "DELETE"}).then((response) => {
                        if(!response.ok){
                            throw new Error('Something went wrong');
                        }
                        window.location.reload(false);
                        })
                        .catch((e) => {
                            console.log(e);
                        });
                    }} className="btn btn-danger">Delete</button></td>
                    </tr>
                )
            })}
        </tbody>
        </table>
        </div>
    )
}


export default ModelList
