import React, { useState, useEffect } from 'react';


export default function SalesHistory(){
    const [salespeople, selectSalesperson] = useState([]);
    const [sales, setSales] = useState([]);
    const [salesperson, setSalesperson] = useState('');

    const fetchData = async () => {
        const salesUrl = "http://localhost:8090/api/sales/";
        const salesResponse = await fetch(salesUrl);

        const salespeopleUrl = "http://localhost:8090/api/salespeople/"
        const salespeopleResponse = await fetch(salespeopleUrl);

        if (salesResponse.ok && salespeopleResponse.ok) {
            const salesData = await salesResponse.json();
            setSales(salesData.sales);

            const salespeopleData = await salespeopleResponse.json();
            selectSalesperson(salespeopleData.salespeople)
        }
    };

    useEffect(() => {
        fetchData();

    }, []);

    const handleSalesperson = (event) => {
        const value = event.target.value
        setSalesperson(value)
    }

    const salesArray = sales.filter(sale => sale.salesperson.employee_id === salesperson).map(filteredSale => {
        return (
            <tr key={ filteredSale.id }>
                <td>{ filteredSale.salesperson.first_name } { filteredSale.salesperson.last_name }</td>
                <td>{ filteredSale.customer.first_name } { filteredSale.customer.last_name }</td>
                <td>{ filteredSale.auto }</td>
                <td>${ filteredSale.price }</td>
            </tr>
        );
    })

    return (
        <>
            <div className="mb-3">
                <select required onChange={ handleSalesperson } value={ salesperson } id="salesperson" className="form-select" name="salesperson">
                    <option value="">Choose an salesperson</option>
                    {salespeople.map(salesperson => {
                        return (
                            <option key={ salesperson.employee_id } value={ salesperson.employee_id }>{ salesperson.employee_id }</option>
                            );
                    })}
                </select>
            </div>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th scope="col">Salesperson Name</th>
                        <th scope="col">Customer</th>
                        <th scope="col">VIN</th>
                        <th scope="col">Price</th>
                    </tr>
                </thead>
                <tbody>
                    { salesArray }
                </tbody>
            </table>
        </>
    )
}
