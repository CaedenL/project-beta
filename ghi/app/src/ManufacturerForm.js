import React, { useState, useEffect } from "react";


function ManufacturerForm() {
    const [formData, setFormData] = useState({
        name: '',
    })

    const handleFormChange = (event) => {
        const value = event.target.value;
        const inputName = event.target.name;
        setFormData({
            ...formData,
            [inputName]: value,
        })
    }

    const handleSubmit = async (event) => {
        event.preventDefault();
        const manufacturerUrl = 'http://localhost:8100/api/manufacturers/';
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            }
        }
        const response = await fetch(manufacturerUrl, fetchConfig);
        if (response.ok) {
            setFormData({
                name: '',
            })
        } else {
            alert("Could not add a manufacturer.")
        }
    };


    return (
        <div className="row">
        <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
            <h1>Add a manufacturer</h1>
            <form onSubmit={handleSubmit} id="add-manufacturer-form">
                <div className="form-floating mb-3">
                    <input value={formData.name} onChange={handleFormChange} placeholder="Manufacturer" required type="text" name="name" id="name" className="form-control"/>
                    <label htmlFor="name">Manufacturer Name</label>
                </div>
                <button className="btn btn-primary">Add</button>
            </form>
            </div>
        </div>
        </div>
    )
}


export default ManufacturerForm
