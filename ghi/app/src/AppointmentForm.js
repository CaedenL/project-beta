import React, { useState, useEffect } from "react";

function AppointmentForm() {

    const [technicians, setTechnicians] = useState([]);
    const [customers, setCustomers] = useState([]);

    const [formData, setFormData] = useState({
        vin: '',
        customer: '',
        date_time: '',
        reason: '',
        technician_id: '',
    })

    const handleFormChange = (event) => {
        const value = event.target.value;
        const inputName = event.target.name;
        setFormData({
            ...formData,
            [inputName]: value,
        })
    }

    const fetchData = async () => {
        const technicianUrl = 'http://localhost:8080/api/technicians/';
        const response = await fetch(technicianUrl);

        const customerUrl = 'http://localhost:8090/api/customers/';
        const response_customer = await fetch(customerUrl);

        if (response.ok) {
            const data = await response.json();
            setTechnicians(data.technicians);
        } else {
            alert("Failed to fetch technicians.")
        }
        if (response_customer.ok) {
            const data = await response_customer.json();
            setCustomers(data.customers);
        } else {
            alert("Failed to fetch customers.")
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    const handleSubmit = async (event) => {
        event.preventDefault();
        const serviceUrl = 'http://localhost:8080/api/appointments/';
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            }
        }
        const response = await fetch(serviceUrl, fetchConfig);
        if (response.ok) {
            setFormData({
                vin: '',
                customer: '',
                date_time: '',
                reason: '',
                technician_id: '',
            })
        } else {
            alert("Failed to create service appointment!")
        }
    };



    return (
        <div className="row">
        <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
            <h1>Make Appointment</h1>
            <form onSubmit={handleSubmit} id="add-service-form">
                <div className="form-floating mb-3">
                    <input value={formData.vin} onChange={handleFormChange} placeholder="VIN Number" required type="text" name="vin" id="vin" className="form-control"/>
                    <label htmlFor="vin">VIN Number</label>
                </div>
                <div className="mb-3">
                    <select value={formData.customer} onChange={handleFormChange} required name="customer" className="form-select">
                        <option value="">Choose customer</option>
                        {customers.map(customer => {
                            return (
                                <option key={customer.id} value={customer.first_name + " " + customer.last_name}>
                                    {customer.first_name + " " + customer.last_name}
                                </option>
                            );
                        })}
                    </select>
                </div>
                <div className="form-floating mb-3">
                    <input value={formData.date_time} onChange={handleFormChange} placeholder="Date & Time" required type="datetime-local" name="date_time" id="date_time" className="form-control"/>
                    <label htmlFor="date_time">Date & Time</label>
                </div>
                <div className="form-floating mb-3">
                    <input value={formData.reason} onChange={handleFormChange} placeholder="Reason" required type="text" name="reason" id="reason" className="form-control"/>
                    <label htmlFor="reason">Reason</label>
                </div>
                <div className="mb-3">
                    <select value={formData.technician_id} onChange={handleFormChange} required name="technician_id" className="form-select">
                        <option value="">Choose technician</option>
                        {technicians.map(technician => {
                            return (
                                <option key={technician.id} value={technician.id}>
                                    {technician.first_name}
                                </option>
                            );
                        })}
                    </select>
                </div>
                <button className="btn btn-primary">Add</button>
            </form>
            </div>
        </div>
        </div>
    )

}


export default AppointmentForm
