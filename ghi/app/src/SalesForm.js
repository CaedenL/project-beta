import React, { useState, useEffect }  from 'react';


export default function SalesForm(){
    const [autos, selectAuto] = useState([]);
    const [auto, setAuto] = useState('');
    const [salespeople, selectSalesperson] = useState([]);
    const [salesperson, setSalesperson] = useState('');
    const [customers, selectCustomer] = useState([]);
    const [customer, setCustomer] = useState('');
    const [price, setPrice] = useState('');

    const fetchData = async () => {
        const vinUrl = "http://localhost:8100/api/automobiles/"
        const vinResponse = await fetch(vinUrl);

        const salespeopleUrl = "http://localhost:8090/api/salespeople/"
        const salespeopleResponse = await fetch(salespeopleUrl);

        const customerUrl = "http://localhost:8090/api/customers/"
        const customerResponse = await fetch(customerUrl);

        if (vinResponse.ok && salespeopleResponse.ok && customerResponse.ok) {
            const vinData = await vinResponse.json();
            selectAuto(vinData.autos);

            const salespeopleData = await salespeopleResponse.json();
            selectSalesperson(salespeopleData.salespeople)

            const customerData = await customerResponse.json();
            selectCustomer(customerData.customers)
        }
    };

    useEffect(() => {
        fetchData();
    }, []);

    const handleAuto = (event) => {
        const value = event.target.value;
        setAuto(value);
    }

    const handleSalesperson = (event) => {
        const value = event.target.value;
        setSalesperson(value);
    }

    const handleCustomer = (event) => {
        const value = event.target.value;
        setCustomer(value);
    }

    const handlePrice = (event) => {
        const value = event.target.value;
        setPrice(value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};
        data.auto = auto;
        data.salesperson = salesperson;
        data.customer = customer;
        data.price = price;

        const saleUrl = "http://localhost:8090/api/sales/";
        const saleFetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const soldUrl = `http://localhost:8100/api/automobiles/${auto}/`;
        const soldFetchConfig = {
            method: "PUT",
            headers: {
                Accept: "application/json",
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({sold: true})

        }

        const saleResponse = await fetch(saleUrl, saleFetchConfig);
        const soldResponse = await fetch(soldUrl, soldFetchConfig)

        if (saleResponse.ok && soldResponse.ok){
            const newSale = await saleResponse.json();
            const nowSold = await soldResponse.json();
            setAuto('');
            setSalesperson('');
            setCustomer('');
            setPrice('');
        }
    }

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Record a New Sale</h1>
                    <form onSubmit={ handleSubmit } id="add-customer-form">
                        <div className="mb-3">
                            <select required onChange={ handleAuto } value={ auto } id="auto" className="form-select" name="auto">
                                <option value="">Choose an automobile VIN</option>
                                {autos.map(auto => {
                                    return (
                                        <option key={ auto.vin } value={ auto.vin }>{ auto.vin }</option>
                                    );
                                })}
                            </select>
                        </div>
                        <div className="mb-3">
                            <select required onChange={ handleSalesperson } value={ salesperson } id="salesperson" className="form-select" name="salesperson">
                                <option value="">Choose an salesperson</option>
                                {salespeople.map(salesperson => {
                                    return (
                                        <option key={ salesperson.employee_id } value={ salesperson.employee_id }>{ salesperson.employee_id }</option>
                                    );
                                })}
                            </select>
                        </div>
                        <div className="mb-3">
                            <select required onChange={ handleCustomer } value={ customer } id="customer" className="form-select" name="customer">
                                <option value="">Choose an customer</option>
                                {customers.map(customer => {
                                    return (
                                        <option key={ customer.id } value={ customer.id }>{ customer.first_name } { customer.last_name }</option>
                                    );
                                })}
                            </select>
                        </div>
                        <div className="form-floating mb-3">
                            <input required onChange={ handlePrice } value={ price } placeholder="Price" type="number" name="price" id="price" className="form-control" />
                            <label htmlFor="price">Price</label>
                        </div>
                        <button className="btn btn-primary">Add</button>
                    </form>
                </div>
            </div>
        </div>
    )
}
