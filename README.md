# CarCar

Team:

* Cape - Service Microservice
* Caeden - Sales Microservice

## Design

    - Navigation menu separated to each category with drop down links to each functional component of that category. (E.g. Appointment nav point opens submenu to appointment list, appoitment creation, and appointment history).
    - Functionality includes viewing complied lists for each category, creating new instances, editing, and deleting created instances.
    - Going for a clean, professional look.

## Service microservice

Models Created and their integrations where applicable:
    AutomobileVO - Model which is populated by data from the inventory microservice and retrieved through the service
        poller. Pulls the existing vins allowing for the addition of a "yes or no" VIP attribute depending on the
        existence of the appointment car's vin in the inventory.
    Technician - Has a one-to-many relationship with the appointment model.
    Status - Model used to attribute "statuses" to the appointment model and has a one-to-many relationship
        with the appointment model as well.
    Appointment - Includes technician and status foreign keys. Status upon http POST is "CREATED", then two class
        methods, cancel and complete add the additional statuses "CANCELLED" and "FINISHED" with separate http PUT
        methods.


## Sales microservice

Models:
    AutomobileVO -
        A value object that pulls the automobile vin from the inventory microservice, allowing it to be connected to
        the sales model.
    Salesperson -
        Attributes include first name, last name, and employee id. The employee id is the primary key for this model.
    Customer -
        Attributes include first name, last name, phone number, and address for a customer and is organized by their id.
    Sales -
        Attributes include price and has customer, salesperson, and automobileVO as foriegn keys.
