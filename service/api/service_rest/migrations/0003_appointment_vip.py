# Generated by Django 4.0.3 on 2023-04-25 14:25

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('service_rest', '0002_alter_status_id'),
    ]

    operations = [
        migrations.AddField(
            model_name='appointment',
            name='VIP',
            field=models.CharField(max_length=3, null=True),
        ),
    ]
