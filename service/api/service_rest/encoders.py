from common.json import ModelEncoder
from service_rest.models import AutomobileVO, Technician, Appointment


class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = ["import_href", 'vin']


class TechnicianListEncoder(ModelEncoder):
    model = Technician
    properties = ['first_name', 'last_name', 'employee_id', 'id']


class TechnicianDetailEncoder(ModelEncoder):
    model = Technician
    properties = ['first_name', 'last_name', 'employee_id', 'id']


class AppointmentEncoder(ModelEncoder):
    model = Appointment
    properties = [
        'date_time',
        'reason',
        'vin',
        'customer',
        'VIP',
        'technician',
        'id',
    ]
    encoders = {
        'technician': TechnicianListEncoder,
    }

    def get_extra_data(self, o):
        return {'status': o.status.name}
